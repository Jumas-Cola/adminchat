<link rel="stylesheet" href="/vendor/laravel-admin-ext/adminchat/css/app.css">

<div>
  @yield('content')
</div>

<script type="text/javascript" src="/js/app.js"></script>
<script src="/vendor/laravel-admin-ext/adminchat/js/app.js"></script>
